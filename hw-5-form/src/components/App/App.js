import React from "react";
import Layout from "../Layout/Layout";
import Musica from "../Musica/Musica";

const App = () => {
  return (
    <Layout>
      <Musica />
    </Layout>
  );
};

export default App;
