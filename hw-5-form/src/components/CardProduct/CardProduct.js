import React from "react";
import { useSelector, useDispatch } from "react-redux";
import PropTypes from "prop-types";
import { ReactComponent as StarIcon } from "../../img/icon/star.svg";
import Button from "../Button/Button";

import {
  setMoadalAddProducts,
  dataProduct,
} from "../../store/modalProducts/actionCreators";
import {
  setFavoriteProductAC,
  countFavoriteProductAC,
} from "../../store/favoriteProduct/actionCreators";
import { setIsFavoriteProductAC } from "../../store/products/actionCreators";
import { findAndSetIsFaforiteInCart } from "../../store/productCart/actionCreators";

import styles from "./CardProduct.module.scss";

const CardProduct = ({
  id,
  productName,
  isActiveFavorite,
  price,
  imageUrl,
  vendorCode,
  color,
}) => {
  const favorites = useSelector((store) => store.favorites.favorite);

  const dispatch = useDispatch();

  return (
    <>
      <div className={styles.container}>
        <div className={styles.wrapperHeader}>
          <p className={styles.productName}>{productName}</p>
          <Button
            textBtn={
              <StarIcon
                className={
                  isActiveFavorite ? styles.starIconActive : styles.starIcon
                }
              />
            }
            hendlerClik={() => {
              dispatch(
                setFavoriteProductAC({
                  id,
                  productName,
                  price,
                  imageUrl,
                  vendorCode,
                  color,
                  isActiveFavorite,
                })
              );
              dispatch(setIsFavoriteProductAC({ id, favorites }));
              dispatch(findAndSetIsFaforiteInCart({ id, favorites }));
              dispatch(countFavoriteProductAC());
            }}
          />
        </div>
        <div className={styles.positionImg}>
          <img
            className={styles.imagesCard}
            src={imageUrl}
            alt={productName}
          ></img>
        </div>
        <p className={styles.priceCard}>Price: {price}$</p>
        <p className={styles.codeProduct}>Vendor-code: {vendorCode}</p>
        <div className={styles.textColorProduct}>
          Color:
          <div
            className={styles.colorProduct}
            style={{ backgroundColor: color }}
          ></div>
        </div>
        <div className={styles.positionBtn}>
          <Button
            bgColor="#067305"
            textBtn="Add to cart"
            hendlerClik={() => {
              dispatch(setMoadalAddProducts(true));
              dispatch(
                dataProduct({
                  id,
                  productName,
                  price,
                  imageUrl,
                  vendorCode,
                  color,
                  isActiveFavorite,
                })
              );
            }}
          />
        </div>
      </div>
    </>
  );
};

CardProduct.propTypes = {
  id: PropTypes.number.isRequired,
  productName: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  imageUrl: PropTypes.string,
  vendorCode: PropTypes.number,
  color: PropTypes.string,
  isActiveFavorite: PropTypes.bool,
  favoriteCardProduct: PropTypes.func,
  addCardInCart: PropTypes.func,
};

CardProduct.defaultProps = {
  imageUrl: "./img/no_icon.png",
  vendorCode: 0,
  color: "#fff",
  favoriteCardProduct: () => {},
  addCardInCart: () => {},
  isActiveFavorite: false,
};

export default CardProduct;
