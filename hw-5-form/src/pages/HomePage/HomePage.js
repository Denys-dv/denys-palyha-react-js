import React from "react";
import { useSelector, useDispatch } from "react-redux";

import CardProductList from "../../components/CardProductList/CardProductList";
import Modal from "../../components/Modal/Modal";

import { setMoadalAddProducts } from "../../store/modalProducts/actionCreators";
import {
  addProductIncartAC,
  cartCountProductAC,
} from "../../store/productCart/actionCreators";

import styles from "./HomePage.module.scss";

function HomePage() {
  const products = useSelector((store) => store.products.products);

  const isOpenModalAdd = useSelector(
    (store) => store.modalAddProducts.isOpenModalAdd
  );

  const dataProduct = useSelector(
    (store) => store.modalAddProducts.dataProduct
  );

  const dispatch = useDispatch();

  const closeModal = () => dispatch(setMoadalAddProducts(false));

  return (
    <>
      <h2 className={styles.heading}>Products</h2>
      <CardProductList product={products} />
      {isOpenModalAdd && (
        <Modal
          header="Add item to cart"
          closeModale={closeModal}
          text={`Do you want to add a product: ${dataProduct.productName}?`}
          actions={
            <>
              <button
                className={styles.btnForModal}
                type="button"
                onClick={() => {
                  dispatch(addProductIncartAC(dataProduct));
                  dispatch(cartCountProductAC());
                  closeModal();
                }}
              >
                Add
              </button>
              <button
                className={styles.btnForModal}
                type="button"
                onClick={closeModal}
              >
                Cancel
              </button>
            </>
          }
        />
      )}
    </>
  );
}

export default HomePage;
