import React from "react";
import { Routes, Route } from "react-router-dom";
import HomePage from "../pages/HomePage/";
import FavoritePage from "../pages/FavoritePage/";
import BascetPage from "../pages/BascetPage/";
import NotFoundPage from "../pages/NotFoundPage/";

function AppRouter({
  product,
  favoriteCardProduct,
  addCardInCart,
  favorite,
  carts,
  incrementCountProductCart,
  decrementCountProductCart,
  deleteCardInBascet,
}) {
  return (
    <Routes>
      <Route
        path="/"
        element={
          <HomePage
            product={product}
            favoriteCardProduct={favoriteCardProduct}
            addCardInCart={addCardInCart}
          />
        }
      />
      <Route
        path="/favorite"
        element={
          <FavoritePage
            favorite={favorite}
            favoriteCardProduct={favoriteCardProduct}
            addCardInCart={addCardInCart}
          />
        }
      />
      <Route
        path="/bascet"
        element={
          <BascetPage
            carts={carts}
            favoriteCardProduct={favoriteCardProduct}
            incrementCountProductCart={incrementCountProductCart}
            decrementCountProductCart={decrementCountProductCart}
            deleteCardInBascet={deleteCardInBascet}
          />
        }
      />
      <Route path="*" element={<NotFoundPage />} />
    </Routes>
  );
}

export default AppRouter;
