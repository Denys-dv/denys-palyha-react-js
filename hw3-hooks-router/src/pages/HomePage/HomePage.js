import React from "react";
import styles from "./HomePage.module.scss";
import CardProductList from "../../components/CardProductList/CardProductList";
import PropTypes from "prop-types";

function HomePage({ product, favoriteCardProduct, addCardInCart }) {
  return (
    <>
      <h2 className={styles.heading}>Products</h2>
      <CardProductList
        product={product}
        favoriteCardProduct={favoriteCardProduct}
        addCardInCart={addCardInCart}
      />
    </>
  );
}

HomePage.propTypes = {
  product: PropTypes.array.isRequired,
  favoriteCardProduct: PropTypes.func.isRequired,
  addCardInCart: PropTypes.func.isRequired,
};

export default HomePage;
