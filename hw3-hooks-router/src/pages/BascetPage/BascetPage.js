import React, { useState } from "react";
import styles from "./BascetPages.module.scss";
import CartProduct from "../../components/CartProduct/CartProduct";
import PropTypes from "prop-types";

function BascetPage({
  carts,
  favoriteCardProduct,
  incrementCountProductCart,
  decrementCountProductCart,
  deleteCardInBascet,
}) {
  return (
    <>
      <h2 className={styles.heading}>Product basket</h2>

      <ul className={styles.container}>
        {carts.length !== 0 ? (
          carts.map(
            ({
              id,
              productName,
              price,
              imageUrl,
              vendorCode,
              color,
              isActiveFavorite,
              count,
            }) => {
              return (
                <li className={styles.listItem} key={id}>
                  <CartProduct
                    id={id}
                    productName={productName}
                    price={price}
                    imageUrl={imageUrl}
                    vendorCode={vendorCode}
                    color={color}
                    isActiveFavorite={isActiveFavorite}
                    favoriteCardProduct={favoriteCardProduct}
                    count={count}
                    incrementCountProductCart={incrementCountProductCart}
                    decrementCountProductCart={decrementCountProductCart}
                    deleteCardInBascet={deleteCardInBascet}
                  />
                </li>
              );
            }
          )
        ) : (
          <h2 className={styles.textHeading}>Product cart is empty!</h2>
        )}
      </ul>
    </>
  );
}

BascetPage.propTypes = {
  carts: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      productName: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
      imageUrl: PropTypes.string,
      vendorCode: PropTypes.number,
      color: PropTypes.string,
      isActiveFavorite: PropTypes.bool,
      count: PropTypes.number,
    })
  ),
  favoriteCardProduct: PropTypes.func.isRequired,
  incrementCountProductCart: PropTypes.func.isRequired,
  decrementCountProductCart: PropTypes.func.isRequired,
  deleteCardInBascet: PropTypes.func.isRequired,
};

BascetPage.defaultProps = {
  imageUrl: "./img/no_icon.png",
  vendorCode: 0,
  color: "#fff",
  isActiveFavorite: false,
  count: 0,
};

export default BascetPage;
