import React from "react";
import styles from "./FavoritePage.module.scss";
import CardProduct from "../../components/CardProduct/CardProduct";
import PropTypes from "prop-types";

function FavoritePage({ favorite, favoriteCardProduct, addCardInCart }) {
  return (
    <>
      <h2 className={styles.heading}>Your favorite products</h2>

      <ul className={styles.container}>
        {favorite.length !== 0 ? (
          favorite.map(
            ({
              id,
              productName,
              price,
              imageUrl,
              vendorCode,
              color,
              isActiveFavorite,
            }) => {
              return (
                <li key={id}>
                  <CardProduct
                    id={id}
                    productName={productName}
                    price={price}
                    imageUrl={imageUrl}
                    vendorCode={vendorCode}
                    color={color}
                    isActiveFavorite={isActiveFavorite}
                    favoriteCardProduct={favoriteCardProduct}
                    addCardInCart={addCardInCart}
                  />
                </li>
              );
            }
          )
        ) : (
          <h2 className={styles.textHeading}>
            There are no favorite product here yet!
          </h2>
        )}
      </ul>
    </>
  );
}

FavoritePage.propTypes = {
  favorite: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      productName: PropTypes.string.isRequired,
      isActiveFavorite: PropTypes.bool,
      price: PropTypes.number.isRequired,
      imageUrl: PropTypes.string,
      vendorCode: PropTypes.number,
      color: PropTypes.string,
    })
  ),

  favoriteCardProduct: PropTypes.func.isRequired,
  addCardInCart: PropTypes.func.isRequired,
};

FavoritePage.defaultProps = {
  imageUrl: "./img/no_icon.png",
  vendorCode: 0,
  color: "#fff",
  isActiveFavorite: false,
};

export default FavoritePage;
