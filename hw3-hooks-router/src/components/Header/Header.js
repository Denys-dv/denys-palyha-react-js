import React from "react";
import HeaderCart from "../HeaderCart/HeaderCart";
import HeaderFavorites from "../HeaderFavorites/HeaderFavorites";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";
import logo from "../../img/logo/pngwing.png";

import styles from "./Header.module.scss";

const Header = ({ countFavorite, cartCount }) => {
  return (
    <>
      <header className={styles.header}>
        <div className={styles.container}>
          <div>
            <NavLink className={styles.headerHome} to="/">
              <img className={styles.logoHeader} src={logo} alt="logo_header" />
            </NavLink>
          </div>
          <ul className={styles.list}>
            <li>
              <NavLink to="/favorite" className={styles.listItemsFavorit}>
                <HeaderFavorites countFavorite={countFavorite} />
              </NavLink>
            </li>
            <li>
              <NavLink to="/bascet" className={styles.listItemsCart}>
                <HeaderCart cartCount={cartCount} />
              </NavLink>
            </li>
          </ul>
        </div>
      </header>
    </>
  );
};

Header.propTypes = {
  countFavorite: PropTypes.number,
  cartCount: PropTypes.number,
};

Header.defaultProps = {
  countFavorite: 0,
  cartCount: 0,
};

export default Header;
