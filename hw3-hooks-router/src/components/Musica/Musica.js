import React, { useState, useEffect } from "react";
import AppRouter from "../../router/AppRouter";

import Header from "../Header/Header";

const Musica = () => {
  const [product, setProduct] = useState([]);
  const [favorite, setFavorite] = useState([]);
  const [carts, setCarts] = useState([]);
  const [countFavorite, setCountFavorite] = useState(0);
  const [cartCount, setCartCount] = useState(0);

  const [isModalOpen, setIsModalOpen] = useState(false);

  const hendlerModalOpen = () => {
    setIsModalOpen(true);
  };

  const hendlerCloseModal = () => {
    setIsModalOpen(false);
  };

  const favoriteCardProduct = (cardProduct) => {
    setFavorite((prevFevorite) => {
      const favorite = [...prevFevorite];
      const index = favorite.findIndex((el) => el.id === cardProduct.id);
      if (index === -1) {
        favorite.push({ ...cardProduct, isActiveFavorite: true });

        setCountFavorite((prevCountFavorit) => {
          return (prevCountFavorit += 1);
        });

        setProduct((prevProduct) => {
          const productsCard = [...prevProduct];
          const indexProd = productsCard.findIndex(
            (el) => el.id === cardProduct.id
          );

          productsCard[indexProd]["isActiveFavorite"] = true;
          localStorage.setItem("product", JSON.stringify(productsCard));
          return productsCard;
        });
      }

      if (index !== -1 && countFavorite >= 0) {
        favorite.splice(index, 1);
        setProduct((prevProduct) => {
          const productsCard = [...prevProduct];
          const indexProd = productsCard.findIndex(
            (el) => el.id === cardProduct.id
          );

          productsCard[indexProd]["isActiveFavorite"] = false;

          setCountFavorite((prevCountFavorit) => {
            return (prevCountFavorit -= 1);
          });
          localStorage.setItem("product", JSON.stringify(productsCard));
          return productsCard;
        });

        setCarts((prevCart) => {
          const carts = [...prevCart];
          const index = carts.findIndex((el) => el.id === cardProduct.id);
          if (carts[index]) {
            carts[index]["isActiveFavorite"] = false;
            localStorage.setItem("carts", JSON.stringify(carts));
          }

          return carts;
        });
      }

      setCarts((prevCart) => {
        const carts = [...prevCart];
        const index = carts.findIndex((el) => el.id === cardProduct.id);
        if (carts[index]) {
          favorite.forEach((el) => {
            if (el.id === carts[index].id) {
              carts[index]["isActiveFavorite"] = true;
            }
          });
          localStorage.setItem("carts", JSON.stringify(carts));
        }
        return carts;
      });

      localStorage.setItem("favorite", JSON.stringify(favorite));
      return favorite;
    });
  };

  const addCardInCart = (hendlerCloseModal, cardProduct) => {
    setCarts((prevCarts) => {
      const carts = [...prevCarts];
      const index = carts.findIndex((el) => el.id === cardProduct.id);
      if (index === -1) {
        carts.push({ ...cardProduct, count: 1 });

        setCartCount((prevCartCount) => {
          prevCartCount += 1;
          hendlerCloseModal();
          localStorage.setItem("cartCount", prevCartCount);
          return prevCartCount;
        });
        favorite.forEach((el) => {
          if (el.id === cardProduct.id) {
            const indexCarts = carts.findIndex(
              (el) => el.id === cardProduct.id
            );

            carts[indexCarts]["isActiveFavorite"] = true;
          }
        });
      } else {
        setCartCount((prevCartCount) => {
          carts[index].count += 1;
          prevCartCount += 1;
          hendlerCloseModal();
          localStorage.setItem("cartCount", prevCartCount);
          return prevCartCount;
        });
      }

      localStorage.setItem("carts", JSON.stringify(carts));

      return carts;
    });
  };

  const incrementCountProductCart = (id) => {
    setCarts((currentCarts) => {
      const carts = [...currentCarts];
      const index = carts.findIndex((el) => el.id === id);

      if (index !== -1) {
        carts[index].count += 1;
        setCartCount((currentCartCount) => {
          currentCartCount += 1;
          localStorage.setItem("cartCount", currentCartCount);
          return currentCartCount;
        });
      }
      localStorage.setItem("carts", JSON.stringify(carts));
      return carts;
    });
  };

  const decrementCountProductCart = (id) => {
    setCarts((currentCarts) => {
      const carts = [...currentCarts];
      const index = carts.findIndex((el) => el.id === id);

      if (index !== -1 && carts[index].count > 1) {
        carts[index].count -= 1;
        setCartCount((currentCartCount) => {
          currentCartCount -= 1;
          localStorage.setItem("cartCount", currentCartCount);
          return currentCartCount;
        });
      }
      localStorage.setItem("carts", JSON.stringify(carts));
      return carts;
    });
  };

  const deleteCardInBascet = (id, hendlerCloseDeleteModal) => {
    setCarts((currentCarts) => {
      const carts = [...currentCarts];
      const index = carts.findIndex((el) => el.id === id);

      if (index !== -1) {
        setCartCount((currentCarts) => {
          currentCarts -= carts[index].count;
          localStorage.setItem("cartCount", currentCarts);
          return currentCarts;
        });
        carts.splice(index, 1);
      }

      localStorage.setItem("carts", JSON.stringify(carts));
      hendlerCloseDeleteModal();
      return carts;
    });
  };

  useEffect(() => {
    (async () => {
      const data = await fetch("./data.json").then((res) => res.json());
      setProduct(data);

      const countItemFavorite = localStorage.getItem("favorite");
      const productItemFavorite = localStorage.getItem("product");
      const cartsCount = localStorage.getItem("cartCount");
      const cartsItem = localStorage.getItem("carts");

      if (countItemFavorite !== null) {
        const arrFavorite = JSON.parse(countItemFavorite);

        setFavorite(arrFavorite);
        setCountFavorite(arrFavorite.length);
      }

      if (productItemFavorite !== null) {
        const arrProduct = JSON.parse(productItemFavorite);
        setProduct(arrProduct);
      }

      if (cartsCount !== null) {
        setCartCount(JSON.parse(cartsCount));
      }

      if (cartsItem !== null) {
        const arrCarts = JSON.parse(cartsItem);
        setCarts(arrCarts);
      }
    })();
  }, []);

  return (
    <>
      <Header countFavorite={countFavorite} cartCount={cartCount} />
      <AppRouter
        product={product}
        favoriteCardProduct={favoriteCardProduct}
        addCardInCart={addCardInCart}
        favorite={favorite}
        carts={carts}
        isModalOpen={isModalOpen}
        hendlerModalOpen={hendlerModalOpen}
        hendlerCloseModal={hendlerCloseModal}
        incrementCountProductCart={incrementCountProductCart}
        decrementCountProductCart={decrementCountProductCart}
        deleteCardInBascet={deleteCardInBascet}
      />
    </>
  );
};

export default Musica;
