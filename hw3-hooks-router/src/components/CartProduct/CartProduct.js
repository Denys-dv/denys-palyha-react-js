import React, { useState } from "react";
import Modal from "../Modal/Modal";
import styles from "./CartProduct.module.scss";
import Button from "../Button/Button";
import { ReactComponent as StarIcon } from "../../img/icon/star.svg";
import PropTypes from "prop-types";

function CartProduct({
  id,
  productName,
  isActiveFavorite,
  price,
  imageUrl,
  vendorCode,
  color,
  favoriteCardProduct,
  count,
  incrementCountProductCart,
  decrementCountProductCart,
  deleteCardInBascet,
}) {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const hendlerModalDeleteOpen = () => {
    setIsModalOpen(true);
  };

  const hendlerCloseDeleteModal = () => {
    setIsModalOpen(false);
  };

  return (
    <>
      <div className={styles.container}>
        <div className={styles.wrapperHeader}>
          <p className={styles.productName}>{productName}</p>
          <div className={styles.containerBtn}>
            <Button
              textBtn={
                <StarIcon
                  className={
                    isActiveFavorite ? styles.starIconActive : styles.starIcon
                  }
                />
              }
              hendlerClik={() => {
                favoriteCardProduct({
                  id,
                  productName,
                  price,
                  imageUrl,
                  vendorCode,
                  color,
                  isActiveFavorite,
                });
              }}
            />
            <button
              className={styles.btnCloseCard}
              onClick={hendlerModalDeleteOpen}
            ></button>
          </div>
        </div>
        <div className={styles.positionImg}>
          <img
            className={styles.imagesCard}
            src={imageUrl}
            alt={productName}
          ></img>
        </div>
        <p className={styles.priceCard}>Price: {price}$</p>
        <p className={styles.codeProduct}>Vendor-code: {vendorCode}</p>
        <div className={styles.textColorProduct}>
          Color:
          <div
            className={styles.colorProduct}
            style={{ backgroundColor: color }}
          ></div>
        </div>
        <div className={styles.positionBtn}>
          <div className={styles.countConteiner}>
            <Button
              textBtn="+"
              bgColor="#138d00"
              hendlerClik={() => {
                incrementCountProductCart(id);
              }}
            />
            <p className={styles.count}>{count}</p>
            <Button
              textBtn="-"
              bgColor="#000fde"
              hendlerClik={() => {
                decrementCountProductCart(id);
              }}
            />
          </div>
        </div>

        {isModalOpen && (
          <Modal
            header="Delete product"
            text={`Are you sure you want to uninstall this product: ${productName}?`}
            closeModale={hendlerCloseDeleteModal}
            actions={
              <>
                <button
                  className={styles.btnForModal}
                  type="button"
                  onClick={() => {
                    deleteCardInBascet(id, hendlerCloseDeleteModal);
                  }}
                >
                  Delete
                </button>
                <button
                  className={styles.btnForModal}
                  type="button"
                  onClick={hendlerCloseDeleteModal}
                >
                  Cancel
                </button>
              </>
            }
          />
        )}
      </div>
    </>
  );
}

CartProduct.propTypes = {
  id: PropTypes.number.isRequired,
  productName: PropTypes.string.isRequired,
  isActiveFavorite: PropTypes.bool,
  price: PropTypes.number.isRequired,
  imageUrl: PropTypes.string,
  vendorCode: PropTypes.number,
  color: PropTypes.string,
  favoriteCardProduct: PropTypes.func,
  count: PropTypes.number,
  incrementCountProductCart: PropTypes.func.isRequired,
  decrementCountProductCart: PropTypes.func.isRequired,
  deleteCardInBascet: PropTypes.func.isRequired,
};

CartProduct.defaultProps = {
  imageUrl: "./img/no_icon.png",
  vendorCode: 0,
  color: "#fff",
  favoriteCardProduct: () => {},
  isActiveFavorite: false,
};

export default CartProduct;
