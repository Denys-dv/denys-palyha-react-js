import React, { PureComponent } from "react";
import styles from "./Modal.module.scss";
import PropTypes from "prop-types";

class Modal extends PureComponent {
  render() {
    const { header, text, closeButton, actions, closeModale } = this.props;

    return (
      <div className={styles.modal}>
        <div className={styles.bgColorModal} onClick={closeModale}></div>
        <div className={styles.container}>
          <div className={styles.headerModal}>
            <h2 className={styles.headerTitle}>{header}</h2>
            {closeButton && (
              <button
                className={styles.btnClose}
                onClick={closeModale}
              ></button>
            )}
          </div>
          <div className={styles.modalBody}>{text}</div>
          <div className={styles.footerBtn}>{actions}</div>
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  header: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  actions: PropTypes.node.isRequired,
  closeModale: PropTypes.func.isRequired,
  closeButton: PropTypes.bool,
};

Modal.defaultProps = {
  closeButton: true,
};

export default Modal;
