import { PureComponent } from "react";
import { ReactComponent as ShopingCart } from "../../img/icon/shoping-cart.svg";
import styles from "./HeaderCart.module.scss";

class HeaderCart extends PureComponent {
  render() {
    const { cartCount } = this.props;

    return (
      <div className={styles.listItemsCart}>
        <ShopingCart className={styles.shopingCard} />
        {cartCount !== 0 ? (
          <span className={styles.countFavorite}>{cartCount}</span>
        ) : (
          ""
        )}
      </div>
    );
  }
}

export default HeaderCart;
