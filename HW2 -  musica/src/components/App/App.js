import React, { Component } from "react";
import Layout from "../Layout/Layout";
import Musica from "../Musica/Musica";

class App extends Component {
  render() {
    return (
      <Layout>
        <Musica />
      </Layout>
    );
  }
}

export default App;
