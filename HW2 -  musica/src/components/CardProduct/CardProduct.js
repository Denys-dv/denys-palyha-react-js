import { Component } from "react";
import styles from "./CardProduct.module.scss";
import Button from "../Button/Button";
import { ReactComponent as StarIcon } from "../../img/icon/star.svg";
import Modal from "../Modal/Modal";
import PropTypes from "prop-types";

class CardProduct extends Component {
  state = {
    isModalOpen: false,
  };

  hendlerModalOpen = () => {
    this.setState({ isModalOpen: true });
  };

  hendlerCloseModal = () => {
    this.setState({ isModalOpen: false });
  };

  render() {
    const { isModalOpen } = this.state;
    const {
      id,
      productName,
      isActiveFavorite,
      price,
      imageUrl,
      vendorCode,
      color,
      favoriteCardProduct,
      addCardInCart,
    } = this.props;

    return (
      <>
        <div className={styles.container}>
          <div className={styles.wrapperHeader}>
            <p className={styles.productName}>{productName}</p>
            <Button
              textBtn={
                <StarIcon
                  className={
                    isActiveFavorite ? styles.starIconActive : styles.starIcon
                  }
                />
              }
              hendlerClik={() => {
                favoriteCardProduct({
                  id,
                  productName,
                  price,
                  imageUrl,
                  vendorCode,
                  color,
                  isActiveFavorite,
                });
              }}
            />
          </div>
          <div className={styles.positionImg}>
            <img
              className={styles.imagesCard}
              src={imageUrl}
              alt={productName}
            ></img>
          </div>
          <p className={styles.priceCard}>Price: {price}$</p>
          <p className={styles.codeProduct}>Vendor-code: {vendorCode}</p>
          <div className={styles.textColorProduct}>
            Color:
            <div
              className={styles.colorProduct}
              style={{ backgroundColor: color }}
            ></div>
          </div>
          <div className={styles.positionBtn}>
            <Button
              bgColor="#067305"
              textBtn="Add to cart"
              hendlerClik={this.hendlerModalOpen}
            />
          </div>

          {isModalOpen && (
            <Modal
              header="Add item to cart"
              closeModale={this.hendlerCloseModal}
              text="Do you want to add a product?"
              actions={
                <>
                  <button
                    className={styles.btnForModal}
                    type="button"
                    onClick={() => {
                      addCardInCart(this.hendlerCloseModal, {
                        id,
                        productName,
                        price,
                        imageUrl,
                        vendorCode,
                        color,
                      });
                    }}
                  >
                    Add
                  </button>
                  <button
                    className={styles.btnForModal}
                    type="button"
                    onClick={this.hendlerCloseModal}
                  >
                    Cancel
                  </button>
                </>
              }
            />
          )}
        </div>
      </>
    );
  }
}

CardProduct.propTypes = {
  id: PropTypes.number.isRequired,
  productName: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  imageUrl: PropTypes.string,
  vendorCode: PropTypes.number,
  color: PropTypes.string,
  isActiveFavorite: PropTypes.bool,
  favoriteCardProduct: PropTypes.func,
  addCardInCart: PropTypes.func,
};

CardProduct.defaultProps = {
  imageUrl: "./img/no_icon.png",
  vendorCode: 0,
  color: "#fff",
  favoriteCardProduct: () => {},
  addCardInCart: () => {},
  isActiveFavorite: false,
};

export default CardProduct;
