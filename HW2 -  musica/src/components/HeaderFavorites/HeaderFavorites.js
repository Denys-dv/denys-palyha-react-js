import { PureComponent } from "react";
import { ReactComponent as StarIcon } from "../../img/icon/star.svg";
import styles from "./HeaderFavorites.module.scss";

class HeaderFavorites extends PureComponent {
  render() {
    const { countFavorite } = this.props;

    return (
      <div className={styles.listItemsFavorit}>
        <StarIcon className={styles.starIcon} />
        {countFavorite !== 0 ? (
          <span className={styles.countFavorite}>{countFavorite}</span>
        ) : (
          ""
        )}
      </div>
    );
  }
}

export default HeaderFavorites;
