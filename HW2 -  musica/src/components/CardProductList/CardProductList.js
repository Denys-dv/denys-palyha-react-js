import { Component } from "react";
import CardProduct from "../CardProduct/CardProduct";
import styles from "./CardProductList.module.scss";
import PropTypes from "prop-types";

class CardProductList extends Component {
  render() {
    const { product, favoriteCardProduct, addCardInCart } = this.props;

    return (
      <div className={styles.container}>
        {product.map(
          ({
            id,
            productName,
            price,
            imageUrl,
            vendorCode,
            color,
            isActiveFavorite,
          }) => (
            <CardProduct
              key={id}
              id={id}
              productName={productName}
              price={price}
              imageUrl={imageUrl}
              vendorCode={vendorCode}
              color={color}
              isActiveFavorite={isActiveFavorite}
              favoriteCardProduct={favoriteCardProduct}
              addCardInCart={addCardInCart}
            />
          )
        )}
      </div>
    );
  }
}

CardProductList.propTypes = {
  product: PropTypes.array,
};

CardProductList.defaultProps = {
  product: [],
};

export default CardProductList;
