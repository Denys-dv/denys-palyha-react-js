import React, { PureComponent } from "react";
import styles from "./Button.module.scss";
import PropTypes from "prop-types";

class Button extends PureComponent {
  render() {
    const { bgColor, textBtn, hendlerClik } = this.props;

    return (
      <button
        type="button"
        style={{ backgroundColor: bgColor }}
        className={styles.btn}
        onClick={hendlerClik}
      >
        {textBtn}
      </button>
    );
  }
}

Button.propTypes = {
  bgColor: PropTypes.string,
  textBtn: PropTypes.node,
  hendlerClik: PropTypes.func,
};

Button.defaultProps = {
  bgColor: "",
  textBtn: "",
  hendlerClikOpenModal: () => {},
};

export default Button;
