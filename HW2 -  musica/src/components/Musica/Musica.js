import { PureComponent } from "react";
import CardProductList from "../CardProductList/CardProductList";
import Header from "../Header/Header";

class Musica extends PureComponent {
  state = {
    product: [],
    favorite: [],
    carts: [],
    countFavorite: 0,
    cartCount: 0,
  };

  favoriteCardProduct = (cardProduct) => {
    this.setState((prevState) => {
      const favorite = [...prevState.favorite];
      const productsCard = [...prevState.product];
      const index = favorite.findIndex((el) => el.id === cardProduct.id);
      const indexProd = productsCard.findIndex(
        (el) => el.id === cardProduct.id
      );

      if (index === -1) {
        favorite.push({ ...cardProduct, isActiveFavorite: true });

        prevState.countFavorite += 1;
        productsCard[indexProd]["isActiveFavorite"] = true;
      }

      if (index !== -1 && prevState.countFavorite >= 0) {
        favorite.splice(index, 1);

        prevState.countFavorite -= 1;
        productsCard[indexProd].isActiveFavorite = false;
      }

      localStorage.setItem("favorite", JSON.stringify(favorite));
      localStorage.setItem("product", JSON.stringify(productsCard));

      return { favorite, productsCard };
    });
  };

  addCardInCart = (hendlerCloseModal, cardProduct) => {
    this.setState((prevCarts) => {
      const carts = [...prevCarts.carts];
      const index = carts.findIndex((el) => el.id === cardProduct.id);
      if (index === -1) {
        carts.push({ ...cardProduct });

        prevCarts.cartCount += 1;
        hendlerCloseModal();
      } else {
        prevCarts.cartCount += 1;
        hendlerCloseModal();
      }

      localStorage.setItem("carts", JSON.stringify(carts));
      localStorage.setItem("cartCount", this.state.cartCount);

      return { carts };
    });
  };

  async componentDidMount() {
    const data = await fetch("./data.json").then((res) => res.json());
    this.setState({ product: data });

    const countItemFavorite = localStorage.getItem("favorite");
    const productItemFavorite = localStorage.getItem("product");
    const cartsCount = localStorage.getItem("cartCount");

    if (countItemFavorite !== null) {
      const arrFavorite = JSON.parse(countItemFavorite);

      this.setState({
        favorite: arrFavorite,
        countFavorite: arrFavorite.length,
      });

      if (productItemFavorite !== null) {
        const arrProduct = JSON.parse(productItemFavorite);
        this.setState({
          product: arrProduct,
        });
      }

      if (cartsCount !== null) {
        this.setState({ cartCount: JSON.parse(cartsCount) });
      }
    }
  }

  render() {
    const { product, countFavorite, cartCount } = this.state;

    return (
      <>
        <Header countFavorite={countFavorite} cartCount={cartCount} />
        <CardProductList
          product={product}
          favoriteCardProduct={this.favoriteCardProduct}
          addCardInCart={this.addCardInCart}
        />
      </>
    );
  }
}

export default Musica;
