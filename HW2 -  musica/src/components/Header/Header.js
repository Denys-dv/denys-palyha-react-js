import React, { PureComponent } from "react";
import HeaderCart from "../HeaderCart/HeaderCart";
import HeaderFavorites from "../HeaderFavorites/HeaderFavorites";
import PropTypes from "prop-types";

import styles from "./Header.module.scss";

class Header extends PureComponent {
  render() {
    const { countFavorite, cartCount } = this.props;
    return (
      <>
        <header className={styles.header}>
          <div className={styles.container}>
            <div className={styles.list}>
              <HeaderFavorites countFavorite={countFavorite} />
              <HeaderCart cartCount={cartCount} />
            </div>
          </div>
        </header>
      </>
    );
  }
}

Header.propTypes = {
  countFavorite: PropTypes.number,
  cartCount: PropTypes.number,
};

Header.defaultProps = {
  countFavorite: 0,
  cartCount: 0,
};

export default Header;
