import React, { Component } from "react";
import Header from "../Header/Header";
import Layout from "../Layout/Layout";

class App extends Component {
  render() {
    return (
      <>
        <Layout>
          <Header />
        </Layout>
      </>
    );
  }
}

export default App;
