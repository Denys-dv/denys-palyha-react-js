import React, { PureComponent } from "react";
import styles from "./Button.module.scss";

class Button extends PureComponent {
  render() {
    const { bgColor, textBtn, hendlerClikOpenModal } = this.props;

    return (
      <button
        type="button"
        style={{ backgroundColor: bgColor }}
        className={styles.btn}
        onClick={hendlerClikOpenModal}
      >
        {textBtn}
      </button>
    );
  }
}

export default Button;
