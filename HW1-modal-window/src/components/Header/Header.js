import React, { Component } from "react";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import styles from "./Header.module.scss";

class Header extends Component {
  state = {
    isModalOpenFirst: false,
    isModalOpenSecond: false,
  };

  hendlerClikOpenModalFirstBtn = () => {
    this.setState({
      isModalOpenFirst: true,
    });
  };

  hendlerClikOpenModalSecondBtn = () => {
    this.setState({
      isModalOpenSecond: true,
    });
  };

  render() {
    const { isModalOpenFirst, isModalOpenSecond } = this.state;

    return (
      <>
        <header className={styles.container}>
          <Button
            bgColor="#037395"
            textBtn="Open first modal"
            hendlerClikOpenModal={this.hendlerClikOpenModalFirstBtn}
          />
          <Button
            bgColor="#01aa23"
            textBtn="Open second modal"
            hendlerClikOpenModal={this.hendlerClikOpenModalSecondBtn}
          />
        </header>
        {isModalOpenFirst && (
          <Modal
            header="Do you want to delete this file?"
            text="Once you delete this file, it won’t be possible to undo this action. 
                    Are you sure you want to delete it?"
            closeButton
            closeModale={() => this.setState({ isModalOpenFirst: false })}
            actions={
              <>
                <button className={styles.btnForModal} type="button">
                  Ok
                </button>
                <button className={styles.btnForModal} type="button">
                  Cancel
                </button>
              </>
            }
          />
        )}

        {isModalOpenSecond && (
          <Modal
            header="Do you want to add a file to the page?"
            text="When you add this file, you can always remove it from your page!!!"
            closeButton
            closeModale={() => this.setState({ isModalOpenSecond: false })}
            actions={
              <>
                <button className={styles.btnForModal} type="button">
                  Comfirm
                </button>
              </>
            }
          />
        )}
      </>
    );
  }
}

export default Header;
