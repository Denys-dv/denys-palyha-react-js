import React, { Component } from "react";
import styles from "./Layout.module.scss";

class Layout extends Component {
  render() {
    const { children } = this.props;
    return <div className={styles.container}>{children}</div>;
  }
}

export default Layout;
